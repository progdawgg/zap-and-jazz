﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nestLand : MonoBehaviour {

    public ParticleSystem birds;
    public bool birdsEnabled;
    public GameObject gameWon;
    public AudioSource birdSound;

    // Update is called once per frame
    private void OnTriggerStay(Collider egg)
    {
        if(egg.tag == "Player")
        {
            Debug.Log("you Won");
            gameWon.SetActive(true);
            var emission = birds.emission;
           emission.enabled = birdsEnabled;
            
        birdSound.Play();

        }
    }
}
