﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	Transform thisTrans;
	Rigidbody thisRigid;

	[SerializeField]
	Transform cameraTrans;

	// Use this for initialization
	void Start () {
		thisTrans = this.transform;
		thisRigid = this.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {

		thisTrans.position += new Vector3 (Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		//thisRigid.AddForce (Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

		//thisRigid.AddForce(cameraTrans.forward *Input.GetAxis("Vertical") * 15);

		//thisRigid.AddForce(cameraTrans.forward *Input.GetAxis("Horizontal") * 15);

		if (thisTrans.position.y < -1000) {
			thisTrans.position = Vector3.zero;
			thisRigid.velocity = Vector3.zero; 
		}
	}
}
