﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl1 : MonoBehaviour {

	Transform thisTrans;

	[SerializeField]
	Transform player;

	// Use this for initialization
	void Start () {
		thisTrans = this.transform;
	}
	
	// Update is called once per frame
	void LateUpdate () {

		thisTrans.position = player.position;

		thisTrans.Rotate(thisTrans.right, Input.GetAxis ("Mouse Y"));

		thisTrans.Rotate(thisTrans.up, Input.GetAxis ("Mouse X"));
		
	}
}
