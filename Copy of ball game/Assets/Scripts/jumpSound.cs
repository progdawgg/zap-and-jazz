﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpSound : MonoBehaviour {

    public AudioSource jumpS;
    public AudioSource landS;

    // Use this for initialization
    private void OnCollisionEnter(Collision collision)
    {
      landS.pitch = Random.Range(0.5f, 1f);
        landS.Play();
    }

    private void OnCollisionStay(Collision collision)
    {
        if (Input.GetKeyDown("space"))
        {
            jumpS.pitch = Random.Range(0.2f, 1.5f);
            jumpS.Play();
        }
    }
}
