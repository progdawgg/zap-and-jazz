﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollowPlayer : MonoBehaviour {

	Transform thisTran;

	[SerializeField]

	Transform player;

	// Use this for initialization
	void Start () {
		thisTran = this.transform;
	}
	
	// Update is called once per frame
	void Update () {
		thisTran.position = player.position; 

		
	}
}
