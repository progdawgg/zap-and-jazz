﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawner : MonoBehaviour {

	Transform thisTrans;


	// Use this for initialization
	void Start () {
		thisTrans = this.transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (thisTrans.position.y < -60) {
			thisTrans.position = Vector3.zero; 
		}
	}
}
