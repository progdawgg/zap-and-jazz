﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {

	[Range(1,10)]
	public float jumpVelocity;
	public ParticleSystem smoke;
	public int emission;

	//bool jumpRequest;


	void Update () {
			}

	void OnCollisionStay(Collision col){
		
		if (Input.GetButtonDown ("Jump")) {

			GetComponent<Rigidbody>().AddForce(Vector3.up * jumpVelocity, ForceMode.Impulse);

			//jumpRequest = true;

		}

	}

	void OnCollisionEnter(Collision col)
	{
		smoke.Emit (emission);

	}
//	void FixedUpdate () {
//		if (jumpRequest) {
//			//GetComponent<Rigidbody>().velocity += Vector3.up * jumpVelocity;
//			GetComponent<Rigidbody>().AddForce(Vector3.up * jumpVelocity, ForceMode.Impulse);
//
//			jumpRequest = false;
//		}
//	}
}
