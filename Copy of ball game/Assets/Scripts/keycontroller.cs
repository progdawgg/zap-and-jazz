﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keycontroller : MonoBehaviour {
    

	void Update () {
        transform.rotation *=
            Quaternion.AngleAxis (Input.GetAxis ("Horizontal") * -25.0f * Time.deltaTime, new Vector3 (0, 0, 1));
        transform.rotation *=
           Quaternion.AngleAxis(Input.GetAxis("Vertical") * 25.0f * Time.deltaTime, new Vector3(1, 0, 0));


		if (transform.rotation.x > 0.005f || transform.rotation.x < -0.005f || transform.rotation.z > 0.005f || transform.rotation.z < -0.005f) 
		{
			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.identity, 2f * Time.deltaTime);
		}
    }
}
