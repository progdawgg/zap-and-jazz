﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelComplete : MonoBehaviour {

    public GameObject[] currentLevel;
    public Transform player;
	//List<GameObject> currentLevel = new List<GameObject>();
	public int levelNum = 0;

    // Use this for initialization
    void Start () {
        //currentLevel = new GameObject[gameObject];
	}

    private void OnTriggerEnter(Collider other)
    {
		
			currentLevel[levelNum].SetActive(false);
		    levelNum += 1;
			currentLevel[levelNum].SetActive(true);
        player.position = Vector3.zero;


    }
}
